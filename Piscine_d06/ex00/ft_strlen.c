/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/04 15:37:51 by jarnolf           #+#    #+#             */
/*   Updated: 2020/03/04 16:50:44 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_strlen(char *str)
{
	int index;
	int cnt;

	index = 0;
	cnt = 0;
	while (str[index] != '\0')
	{
		index++;
		cnt++;
	}
	return (0);
}
