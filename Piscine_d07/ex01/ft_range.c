/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/05 20:02:42 by jarnolf           #+#    #+#             */
/*   Updated: 2020/03/05 21:39:26 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		*ft_range(int min, int max)
{
	int *r;
	int m;

	m = 0;
	r = malloc(sizeof(int) * (max - min + 1));
	if (min > max || r == 0)
		return (0);
	while (min < max)
	{
		r[m] = min;
		m++;
		min++;
	}
	return (r);
}
