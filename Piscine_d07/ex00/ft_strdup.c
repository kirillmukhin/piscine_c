/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/05 14:38:21 by jarnolf           #+#    #+#             */
/*   Updated: 2020/03/05 20:09:11 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

char	*ft_strdup(char *src)
{
	int		i;
	int		a;
	char	*dest;

	i = 0;
	a = 0;
	while (src[i] != '\0')
		i++;
	dest = malloc(sizeof(*src) * (i + 1));
	while (src[a] != '\0')
	{
		dest[a] = src[a];
		a++;
	}
	return (dest);
}
