/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_map.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/09 19:59:29 by jarnolf           #+#    #+#             */
/*   Updated: 2020/03/10 11:44:11 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		*ft_map(int *tab, int length, int (*f)(int))
{
	int index;
	int *output;

	index = 0;
	output = malloc(sizeof(int) * length);
	while (index < length)
	{
		output[index] = f(tab[index]);
		index++;
	}
	return (output);
}
