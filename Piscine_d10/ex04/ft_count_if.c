/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_count_if.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/10 12:53:45 by jarnolf           #+#    #+#             */
/*   Updated: 2020/03/10 21:08:44 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_count_if(char **tab, int (*f)(char*))
{
	int index;
	int counter;

	index = 0;
	counter = 0;
	while (tab[index] != 0)
	{
		if ((*f)(tab[index]) == 1)
			counter++;
		index++;
	}
	return (counter);
}
