/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_head.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/12 12:07:22 by jarnolf           #+#    #+#             */
/*   Updated: 2020/03/12 13:08:17 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_HEAD_H
# define FT_HEAD_H
# include <unistd.h>
# include <fcntl.h>
# define BUF_SIZE 42

void ft_putchar(char c);
void ft_putstr(char *str);

#endif
