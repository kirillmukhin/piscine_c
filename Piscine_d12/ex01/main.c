/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/12 16:45:11 by jarnolf           #+#    #+#             */
/*   Updated: 2020/03/12 22:01:31 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#define BUF_SIZE 30

void	ft_putstr(char *s, int err)
{
	int index;

	index = 0;
	while (s[index] != '\0')
	{
		if (err == 1)
			write(1, &s[index], 1);
		else
			write(2, &s[index], 1);
		index++;
	}
}

int		file_display(char *name)
{
	int		fd;
	int		ret;
	char	buf[BUF_SIZE + 1];

	fd = open(name, O_RDONLY);
	if (fd < 0)
	{
		ft_putstr("ft_cat: ", 2);
		ft_putstr(name, 2);
		if (errno == EACCES)
			ft_putstr(":  Permission denied\n", 2);
		else if (errno == EISDIR)
			ft_putstr(":  Is a directory\n", 2);
		else
			ft_putstr(":  No such file or directory\n", 2);
		return (1);
	}
	while ((ret = read(fd, buf, BUF_SIZE)))
	{
		buf[ret] = '\0';
		ft_putstr(buf, 1);
	}
	if (close(fd) == -1)
		return (1);
	return (0);
}

int		main(int argc, char **argv)
{
	int		i;
	int		ret;
	char	buf[BUF_SIZE + 1];

	i = 1;
	if (argc == 1)
	{
		while ((ret = read(0, buf, BUF_SIZE)))
		{
			buf[ret] = '\0';
			ft_putstr(buf, 1);
		}
	}
	while (argv[i] != 0)
	{
		file_display(argv[i]);
		i++;
	}
	return (0);
}
